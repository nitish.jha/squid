variable "region" {
  type        = string
  default     = "us-east-2"
  description = "Enter the region in which Infra will created"
}

variable "default_tags" {
  type        = map(any)
  default     = { ENV = "Proxy-server", Owner = "Nitish", company = "Opstree" }
  description = "Enter the tags"
}
variable "az" {
  type        = list(string)
  default     = ["us-east-2a", "us-east-2b", "us-east-2c"]
  description = "Enter the Availability Zones"
}
#variable "private_subnets" {
#  type = list(string)
#  default = ["12.0.1.0/24", "12.0.2.0/24", "12.0.3.0/24"]
#  description = "Enter the private subnets cidr"
#}

variable "public_subnets" {
  type        = list(string)
  default     = ["12.0.101.0/26", "12.0.102.0/26"]
  description = "Enter the public subnet cidr"
}

variable "vpc_cidr" {
  type        = string
  default     = "10.0.0.0/16"
  description = "Enter the vpc cidr"
}

variable "vpc_name" {
  type        = string
  default     = "squid-vpc"
  description = "Enter vpc name"
}

variable "squid_sg_name" {
  type        = string
  default     = "squidproxy-sg"
  description = "Enter name of security group"
}

variable "ssh_sg_name" {
  type        = string
  default     = "ssh-sg"
  description = "Enter name of security group"
}

variable "nlb_name" {
  type        = string
  default     = "squid-nlb"
  description = "Enter nlb name"
}

variable "target_group" {
  type        = list(any)
  default     = [{ name = "sqtg", backend_protocol = "TCP", backend_port = 3128, target_type = "instance" }]
  description = "Enter name, Enter backend protocol, port, target type"
}

variable "http_listener" {
  type        = list(any)
  default     = [{ port = 80, protocol = "TCP", target_group_index = 0 }]
  description = "Enter listenerport, protocol,targetgroupinder"
}

variable "resource_tags" {
  type        = map(any)
  default     = { ENV = "proxy-server", Owner = "Nitish", company = "Opstree" }
  description = "Enter tags"
}

variable "asg_name" {
  type        = string
  default     = "squid-proxy"
  description = "Enter autoscaling group name"
}

variable "min_size" {
  type        = number
  default     = 1
  description = "Enter minimum instance size"
}

variable "max_size" {
  type        = number
  default     = 3
  description = "Enter maximum instance size"
}

variable "desired_capacity" {
  type        = number
  default     = 2
  description = "Enter desired instance size"
}

variable "health_check_type" {
  type        = string
  default     = "EC2"
  description = "Enter health check type"
}

variable "lt_name" {
  type        = string
  default     = "squid-template"
  description = "Enter launch template name"
}

variable "image_id" {
  type        = string
  default     = "ami-00399ec92321828f5"
  description = "Enter ami"
}

variable "instance_type" {
  type        = string
  default     = "t2-micro"
  description = "Enter instance type"
}

variable "key_name" {
  type        = string
  default     = "ansiblekey"
  description = "Enter key name"

}

variable "domain_name" {
  type        = string
  default     = "abhininja.tk"
  description = "Enter domain name"
}

variable "policy_name" {
  type        = string
  default     = "target-traclking-policy"
  description = "Enter policy name"
}

variable "policy_type" {
  type        = string
  default     = "TargetTrackingScaling"
  description = "Enter policy type"
}

variable "target_value" {
  type        = number
  default     = 60.0
  description = "Enter value"
}

variable "metric_type" {
  type        = string
  default     = "ASGAverageCPUUtilization"
  description = "Enter metric type"
}

variable "ingress_with_cidr_blocks_squid" {
  type        = list(any)
  default     = [{ from_port = 3128, to_port = 3128, protocol = "tcp", description = "For squid", cidr_blocks = "0.0.0.0/0" }]
  description = "Enter the ports and cidrs you want to open"
}

variable "ingress_with_cidr_blocks_ssh" {
  type        = list(any)
  default     = [{ from_port = 22, to_port = 22, protocol = "tcp", description = "For Ssh", cidr_blocks = "0.0.0.0/0" }]
  description = "Enter the ports and cidrs you want to open"
}
