default_tags                   = { ENV = "Proxy-server", Owner = "Nitish", company = "Opstree" }
vpc_cidr                       = "12.0.0.0/16"
az                             = ["us-east-2a", "us-east-2b", "us-east-2c"]
public_subnets                 = ["12.0.101.0/24", "12.0.102.0/24"]
vpc_name                       = "squid-vpc"
squid_sg_name                  = "squidproxy-sg"
ssh_sg_name                    = "ssh_sg"
nlb_name                       = "squid-nlb"
target_group                   = [{ name = "sqtg", backend_protocol = "TCP", backend_port = 3128, target_type = "instance" }]
http_listener                  = [{ port = 80, protocol = "TCP", target_group_index = 0 }]
resource_tags                  = { ENV = "proxy-server", Owner = "Nitish", company = "Opstree" }
asg_name                       = "squid-proxy"
min_size                       = 1
max_size                       = 3
desired_capacity               = 1
health_check_type              = "EC2"
lt_name                        = "squid-template"
image_id                       = "ami-00399ec92321828f5"
instance_type                  = "t2.micro"
key_name                       = "ansiblekey"
domain_name                    = "abhininja.tk"
policy_name                    = "target-traclking-policy"
policy_type                    = "TargetTrackingScaling"
target_value                   = 60.0
ingress_with_cidr_blocks_squid = [{ from_port = 3128, to_port = 3128, protocol = "tcp", description = "Squid-port", cidr_blocks = "0.0.0.0/0" }]
ingress_with_cidr_blocks_ssh   = [{ from_port = 22, to_port = 22, protocol = "tcp", description = "For Ssh", cidr_blocks = "0.0.0.0/0" }]

