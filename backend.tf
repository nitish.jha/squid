
terraform {
  backend "s3" {
    bucket = "terraform-remote-squid"
    key    = "terraform/terraform.tfstate"
    region = "us-east-2"
  }
}

