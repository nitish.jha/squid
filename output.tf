output "vpc" {
  description = "ID of project VPC"
  value       = module.vpc.vpc_id
}

output "squid_sg_group" {
  description = "sg info"
  value       = module.squidproxy-security-group
}

output "ssh_sg_group" {
  description = "ssh info"
  value       = module.ssh-security-group
}

output "nlb_targetgroup" {
  description = "nlb tg info"
  value       = module.nlb.target_group_arns
}

output "autoscaling_group" {
  description = "asg info"
  value       = module.asg
}

# output "autoscaling_group" {
#   description = "lt_userdata"
#   value = module.asg.user_data
# }
output "aws_route53_zone" {
  description = "route53info"
  value       = data.aws_route53_zone.selected
}

output "aws_route53_record" {
  description = "route53info"
  value       = resource.aws_route53_record.www
}


